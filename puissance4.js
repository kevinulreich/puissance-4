$(document).ready(function()
{
	class grille
	{
		constructor(color1, color2, width, height){
			this.ROWS = height;
			this.COLS = width;
			this.color1 = color1;
			this.color2 = color2;
			this.selector = $('#puissance4');
			this.grid();
		}

		grid()
		{
			var selector = this.selector;
			for (var row = 0; row < this.ROWS; row++)
			{
				var divrow = $('<div></div>',
				{
					class:"row"+row+"",
				});
				divrow.css(
				{
					"display":"flex"
				});
				for (var col = 0; col < this.COLS; col++)
				{
					var divcol = $('<div></div>', 
					{
						class: "col"+col+"",
					});
					divcol.css(
					{ 
						"background-color": "blue", 
						"width":"100px", 
						"height":"100px"
					});
					var hole = $("<div></div",
					{
						class: "placeholder placeholder"+col+"",
					});
					hole.css({
						"border-radius": "90px",
						"width":"90px",
						"height":"90px",
						"background-color": "white"
					});
					divrow.append(divcol);
					divcol.append(hole);
					selector.append(divrow);
				}
			}
		}

		arrayCheck()
		{
			//l'ensemble des jetons;
			var arr = new Array();
			for(let row = 0; row < this.ROWS; row++)
			{
				arr[row] = new Array ();
				for(let col = 0; col < this.COLS; col++)
				{
					arr[row][col] = null;
				}
			}
			return arr;
		}

		direction(e)
		{
			var valcol = e.parents('div').attr('class');
			var valrow = e.parents('div').parents('div').attr('class');
			var valCol = parseFloat(valcol.slice(3));
			var valRow = parseFloat(valrow.slice(3));
			var tab = [valCol, valRow];
			return tab;
		}

		checkVictory(player, check, currentClickCol)
		{
				var rows = this.ROWS;
				var selector = this.selector;
				var winHorizontal;
				var winVertical = 0;
				var winDiagonal = 0;
				for (let row = rows-1; row > 0; row--)
				{
					winHorizontal = 0;

					// Cas de victoire horizontal
					check[row].forEach((score)=>
					{
						if(score == player)
							winHorizontal++;
						else
							winHorizontal = 0;
						if(winHorizontal == 4)
						{
							alert("le joueur "+player+" a gagné (horizontal) !");
							location.reload(true);
						}
					});

					// Cas de victoire vertical
					if (check[row][currentClickCol] == player)
						winVertical++;
					else
						winVertical = 0;

					if (winVertical == 4)
					{
						alert("le joueur "+player+" a gagné (vertical) !");
						location.reload(true);
					}
			
				// Case de victoire diagonal        
				check[row].forEach((score, index)=>
				{

					if(score == player)
					{
							winDiagonal = 1;

							let indexCol = index;
							let indexRow = row;

						// verification diagonale vers la droite
						while(check[indexRow-1][indexCol+1] == player)
						{          
							winDiagonal++;

							indexCol++;
							indexRow--;
						}
						if(winDiagonal >= 4)
						{
							alert("le joueur "+player+" a gagné (diagonale vers la droite) !");
							location.reload(true);
						}
						
						winDiagonal = 1;
						
						// verification diagonale vers la gauche
						while(check[indexRow-1][indexCol-1] == player)
						{
							winDiagonal++;

							indexCol--;
							indexRow--;
						}
						if(winDiagonal >= 4) {
							alert("le joueur "+player+" a gagné (diagonale vers la gauche) !");
							location.reload(true);
						}
					}
					else
						winDiagonal = 0;
				});
    		}
		}

		jeton()
		{
			var player = 1;
			var obj = this;
			var check = this.arrayCheck();
			var rows = this.ROWS;
			var color1player = this.color1;
			var color2player = this.color2;

			$('.placeholder').click('col',function(event)
			{
				var currentClickPosition = obj.direction($(this));

				for (let rowPosition = rows-1; rowPosition >= 0; rowPosition--) 
				{
					var currentClickCol = currentClickPosition[0];

					if(check[rowPosition][currentClickCol] == null)
					{
						check[rowPosition][currentClickCol] = player;

						var indexCol = currentClickCol;
						var _row = $('.row'+rowPosition);
						var placeholderToFill = _row.find('.placeholder' + indexCol);

						obj.checkVictory(player, check, currentClickCol);

						var color;
						if(player == 1) {
							color = color1player;
							player = 2;
						}
						else {
							color = color2player;
							player = 1;
						}

						placeholderToFill.css("background-color",color);

						break;
					}
				}


			});
		}
	}

	var puissance4 = 0;

	$('#envoyer').click(function(event){
		event.preventDefault();
		puissance4++;
		console.log(puissance4)
		if (puissance4 > 1) {
			$('#puissance4').empty();
			puissance4 = 1;
			console.log(puissance4);
		}
		var color1 = $('#color1').val();
		var color2 = $('#color2').val();
		var width = $('#width').val();
		var height = $('#height').val();
		if (width > 15) {
			width = 15;
		}
		if (width < 4) {
			width = 4;
		}
		if (height > 15) {
			height = 15;
		}
		if (height < 4) {
			height = 4;
		}
		var jeu = new grille(color1, color2, width, height);
		jeu.jeton();
	});
});
